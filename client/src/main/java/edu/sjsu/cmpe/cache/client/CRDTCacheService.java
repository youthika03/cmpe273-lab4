package edu.sjsu.cmpe.cache.client;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.json.JSONObject;

import com.google.common.collect.HashMultimap;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.async.Callback;
import com.mashape.unirest.http.exceptions.UnirestException;

public class CRDTCacheService implements CacheServiceInterface {
	private final String cacheServerUrls[];

	public CRDTCacheService(String... serverUrl) {
		this.cacheServerUrls = serverUrl;
	}

	@Override
	public String get(long key) {
		System.out.println("Doing Async get to All servers...");
		HashMultimap<String, Integer> valueRank = HashMultimap.create();
		int index = 0;

		for (final String url : cacheServerUrls) {
			try {
				HttpResponse<JsonNode> httpResponse = Unirest.get(url + "/cache/{key}").header("accept", "application/json")
						.routeParam("key", Long.toString(key)).asJson();
				if (httpResponse.getCode() == 200) {
					JSONObject jsonObject = new JSONObject(httpResponse.getBody().toString());
					String recvdValue = jsonObject.optString("value");
					valueRank.put(recvdValue, index);
				} else {
					valueRank.put("null", index);
				}
			} catch (UnirestException e) {
				e.printStackTrace();
			}
			index++;
		}

		System.out.println("Recvd " + valueRank);

		// find right value by majority
		String rightValue = null;
		for (String k : valueRank.keySet()) {
			if (valueRank.get(k).size() > (cacheServerUrls.length / 2.0)) {
				rightValue = k;
			}
		}

		if (rightValue != null) {
			// Correct wrong ones
			for (String k : valueRank.keySet()) {
				if (!rightValue.equals(k)) {
					for (Integer i : valueRank.get(k)) {
						try {
							System.out.println("Correcting value for " + cacheServerUrls[i] + " from " + k + " to " + rightValue);
							Unirest.put(cacheServerUrls[i] + "/cache/{key}/{value}").header("accept", "application/json").routeParam("key", k)
									.routeParam("value", rightValue).asString();
						} catch (UnirestException e) {
							e.printStackTrace();
						}
					}

				}
			}
		}

		return rightValue;
	}

	public boolean crdtWrite(long key, String value) {
		ArrayList<Future<HttpResponse<JsonNode>>> fl = new ArrayList<Future<HttpResponse<JsonNode>>>();
		System.out.println("Doing Async Put to All servers...");
		for (final String url : cacheServerUrls) {
			Future<HttpResponse<JsonNode>> future = Unirest.put(url + "/cache/{key}/{value}").header("accept", "application/json")
					.routeParam("key", Long.toString(key)).routeParam("value", value).asJsonAsync(new Callback<JsonNode>() {

						public void failed(UnirestException e) {
							System.out.println("The request has failed on " + url);

						}

						public void completed(HttpResponse<JsonNode> response) {
							int code = response.getCode();
							JsonNode body = response.getBody();
							InputStream rawBody = response.getRawBody();
						}

						public void cancelled() {
							System.out.println("The request has been cancelled on " + url);
						}

					});
			fl.add(future);
		}
		int successCounter = 0;
		List<Integer> successfullIndeces = new ArrayList<Integer>();
		int index = 0;
		for (Future<HttpResponse<JsonNode>> f : fl) {
			try {
				if (f.get().getCode() == 200) {
					successCounter++;
					successfullIndeces.add(index);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
			index++;
		}
		System.out.println("Recvd " + successCounter + " Successfull response(s)");

		if (successCounter < (fl.size() / 2.0)) {
			// delete
			for (Integer i : successfullIndeces) {
				try {
					System.out.println("deleting keys on " + cacheServerUrls[i]);
					Unirest.delete(cacheServerUrls[i] + "/cache/{key}").header("accept", "application/json").routeParam("key", Long.toString(key))
							.asString();
				} catch (UnirestException e) {
					e.printStackTrace();
				}
			}
			return false;
		}
		return true;
	}

	public boolean put(long key, String value) {
		ArrayList<Future<HttpResponse<JsonNode>>> fl = new ArrayList<Future<HttpResponse<JsonNode>>>();
		System.out.println("Doing Async Put to All servers...");
		for (final String url : cacheServerUrls) {
			Future<HttpResponse<JsonNode>> future = Unirest.put(url + "/cache/{key}/{value}").header("accept", "application/json")
					.routeParam("key", Long.toString(key)).routeParam("value", value).asJsonAsync(new Callback<JsonNode>() {

						public void failed(UnirestException e) {
							System.out.println("The request has failed on " + url);

						}

						public void completed(HttpResponse<JsonNode> response) {
							int code = response.getCode();
							JsonNode body = response.getBody();
							InputStream rawBody = response.getRawBody();
						}

						public void cancelled() {
							System.out.println("The request has been cancelled on " + url);
						}

					});
			fl.add(future);
		}
		int successCounter = 0;
		List<Integer> successfullIndeces = new ArrayList<Integer>();
		int index = 0;
		for (Future<HttpResponse<JsonNode>> f : fl) {
			try {
				if (f.get().getCode() == 200) {
					successCounter++;
					successfullIndeces.add(index);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
			index++;
		}
		System.out.println("Recvd " + successCounter + " Successfull response(s)");

		return true;
	}
}
